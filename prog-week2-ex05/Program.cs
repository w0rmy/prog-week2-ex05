﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_week2_ex05
{
    class Program
    {
        static void Main(string[] args)
        {
            var welcome = "Welcome to my amazing math quiz. Type 'true' or 'false' to answer the questions.";

            var score = 0;

            var q1 = "1 + 1 = 5";
            var q2 = "2 + 2 = 4";
            var q3 = "1 + 1 = 2";
            var q4 = "2 + 2 = 5";
            var q5 = "1 + 2 = 5";

            var answer = true;

            Console.WriteLine($"{welcome}");

            if (answer == true)
            {
                Console.WriteLine($"{q1}");
                if (Console.ReadLine() == "false")
                {
                    score++;
                    answer = true;
                    //Put in for debugging
                    //Console.WriteLine($"{score}"); 
                }
                else
                {
                    Console.WriteLine($"Your score was {score}. Thanks for playing!");
                    answer = false;
                }
            }
            if (answer == true)
            {
                Console.WriteLine($"{q2}");
                if (Console.ReadLine() == "true")
                    {
                    score++;
                    answer = true;
                    //Put in for debugging
                    //Console.WriteLine($"{score}"); 
                    }
                else
                {
                    Console.WriteLine($"Your score was {score}. Thanks for playing!");
                    answer = false;
                }
            }
            if (answer == true)
            {
                Console.WriteLine($"{q3}");
                if (Console.ReadLine() == "true")
                {
                    score++;
                    answer = true;
                    //Put in for debugging
                    //Console.WriteLine($"{score}"); 
                }
                else
                {
                    Console.WriteLine($"Your score was {score}. Thanks for playing!");
                    answer = false;
                }
            }
            if (answer == true)
            {
                Console.WriteLine($"{q4}");
                if (Console.ReadLine() == "false")
                {
                    score++;
                    answer = true;
                    //Put in for debugging
                    //Console.WriteLine($"{score}"); 
                }
                else
                {
                    Console.WriteLine($"Your score was {score}. Thanks for playing!");
                    answer = false;
                }
            }
            if (answer == true)
            {
                Console.WriteLine($"{q5}");
                if (Console.ReadLine() == "false")
                {
                    score++;
                    answer = true;
                    //Put in for debugging
                    //Console.WriteLine($"{score}"); 
                }
                else
                {
                    Console.WriteLine($"Your score was {score}. Thanks for playing!");
                    answer = false;
                }
            }
            if (answer == true)
            {
                Console.WriteLine($"You got them all correct! Your score was {score}");
            }
        }
    }
}
